./0_remove_comments example.shorthand    > comments_removed.txt
./1_expanddate      comments_removed.txt > date_expanded.txt
./2_convert_to_csv  date_expanded.txt    > date_expanded.csv
./3_explicify_items date_expanded.csv    > items_expanded.csv
cp items_expanded.csv final.csv
