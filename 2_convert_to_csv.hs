#!/usr/bin/env stack
-- stack --resolver lts-12.5 script

{--
    convert pretty printed(new column indicated by position of new column name) table
    to csv

    to compile: `stack ghc -- 2_convert_to_csv.hs`
                `./2_convert_to_csv file.txt`
--}

import Data.List (intercalate)
import Data.List.Split (splitPlaces, wordsBy)
import System.Environment

type Line = String

main :: IO ()
main = do
  args <- getArgs
  file <- readFile (head args)
  let (coltypes:colnames:ls) = lines file
  let lp = preprocess (colnames:ls)
  let table = fwt2csv lp          -- ::[Row]
  mapM_ putStrLn (coltypes:table)
  

-- and pad every line, so every col is represented(if col not reached we get err in transpose)
preprocess :: [Line] -> [Line]
preprocess ls = map pad ls
            where 
              --rightpad line to maximum length occuring among all
              pad line  = rpad line maxLen 
                   where maxLen = maximum $ map length ls
                                                  
              rpad :: String -- ^ given string
                   -> Int    -- ^ desired min length
                   -> String
              rpad string len = take len $ string++repeat ' '

-- fixed width table to csv
fwt2csv :: [String] -> [String]
fwt2csv lines = map (commy . splitPlaces lengths) lines
 where
  lengths = dissect $ head lines



--calc lengths of the chunks(colname + spaces) for splitplaces
dissect :: Line -> [Int]
dissect line = zipWith (length ... (++)) names spaces
          where names = wordsBy (==' ') line
                spaces= wordsBy (/=' ') (line++" ") --add space to ensure same number of elements in case line ends on word

                
(...) :: (b -> c) -> (a1 -> a2 -> b) -> a1 -> a2 -> c
(...) = (.) . (.) -- blackbird

commy :: [String] -> String
commy = intercalate "," . map rstrip

--could be imported from Data.String.Utils (MissingH) but has many dependencies
-- drops all trailing whitespace
rstrip :: String -> String
rstrip = reverse . dropWhile (==' ') . reverse
