Haskell scripts to convert data from easy to type format to csv readable by [self-quantification-shiny][1]

# use case

A user wants to record daily activities and visualize them. For visualization they pass a csv file to their preferred visualisation solution.
Recording the data happens on a smart phone because it is always available and the data can be passed to a computer automatically(e.g. with syncthing).
But typing in csv is too cumbersome, and a custom app would be too inflexible.
The scripts in this repository provide a way to convert data recorded in a convenient shorthand format to csv.

# Format

Example:

    [Date "date",TimeUp "getuptime",BinaryFac "jogging",MultiFac ["french_read","french_listen","french_speak"] "rls",HexMultiFac ["teeth_floss","teeth_dinner","teeth_lunch","teeth_breakfast"],Time "gobedtime",Parse [OneOfManyFac "meat" ["cow","pork","chicken","fish","vegetarian","vegan"] [["u"],["p"],["c"],["f"],["v"],["V"]],BinaryPFac "vacuuming" ["vac"]]]
    d  up   j fr  t bed   parse
    2015-09
     8 8:11   r   5 23:14 v
     9 A:16 O     1 23:06 v
    10 7:39   rls 1 22:55 c vac
          ...
    29 6:01   l   0 00:34 f
    #  up   j fr  t bed   parse
    personal notes


## Line 1

The first line specifies the operations to convert columns of the shorthand table to csv. This allows for flexibility as different users can record different things or adapt the things they record without having to change program code. The line is valid Haskell code, a list of types specified in 3_explicify_items.hs. The order of the list is free with two exceptions:

 - `Date` must be the first element
 - `Parse` must be the last element.

The first line may not be very easy to read but that is not its purpose! Once you've got yours right you can stop worrying about it. 

## Line 2

The second line are the column names for shorthand notation. The shorthand format is a fixed width table and the column names indicate at which position the columns start
d  up   j fr  t bed   parse

## Line 3

The third line must be the current year, month as "YYYY-MM". This will be prepended to the following lines by the scripts. Every new month also needs a line specifying "YYYY-MM" before it.

## Content

Items can be filled at the position indicated by line 2 and in the format specified in line 1.  
The first item must be the day of the month as mentioned above.  
The second item here is the time at which the user gets up. The hour is hex encoded because the user is assumed to get up between 00:00 and 15:59.  
The third item is binary and denotes jogging. If the user goes for a jog they put in a "O" (or whatever they want e.g "X") otherwise they just leave it empty.
The fourth item comprises 3 columns.
The user wants to learn french and resolves to read, listen to, and speak a little french every day.
Depending on what they get to they put in 'r', 'l' and 's' in whichever order they want.
The fifth item, `t`, deals with brushing teeth after breakfast, lunch and dinner, and flossing once a day.
Since there is a natural order to those we can encode them hexadecimal as 1, 2, 4 and 8.
In the example above on September 8th the user brushed their teeth after breakfast (1) and dinner (4).   
The penultimate item is bed time.
This time it is not compressed.
The last item is a string that comprises several items that are parsed.
These items are occurring only once in a while for example. (or the user has added them on the fly and will adapt the first line before the next evaluation) 
At the time the items have to be separated by spaces but can be ordered at will.

## Last line

As the file can get quite long it is recommended to put the second line, the column names at the end of the table (as a comment of course!) to ensure consistent alignment.
If the second line is repeated every line after it will be dropped.

## Comments

Everything after a `#` will be filtered out. Everything inside parenthesis will also be filtered out. 
Additionally every line after either "#EOF" at the start of the line or the second line again as described will be dropped to give the user space for other notes.


[1]: https://gitlab.com/timo-a/self-quantification-shiny/