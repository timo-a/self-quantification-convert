#!/usr/bin/env stack
-- stack --resolver lts-12.5 script

{-
    

    to compile: `stack ghc -- 1_expanddate.hs`
                `./1_expanddate file.txt`
-}

import Data.Char
import System.Environment

main :: IO ()
main = do
  args <- getArgs
  file <- readFile (head args)
  let (coltypes:colnames:ls) = lines file
  let lp = coltypes:stretchHeader colnames:expandDate ls
  mapM_ putStrLn lp

type Line = String

-- date column gets wider -> date header must also get wider
stretchHeader :: Line -> Line
stretchHeader (_:hs) = "date     " ++ hs


--TODO: use parser?
expandDate :: [Line] -> [Line]
expandDate = concatMap prependYYYY_MMs . chunkIntoMonths
    where
      -- for a given 'chunk tuple' (a,b) prepend a to every line in b
      prependYYYY_MMs :: (Line,[Line]) -> [Line]
      prependYYYY_MMs (l,ls) = map (prependYYYY_MM l) ls
      
      -- Prepend a string of yyyy_mm to a regular line string
      prependYYYY_MM :: String -- ^ a date string "yyyy-mm"
                     -> String -- ^ a string starting with day of the month "[ 12][0-9] ..."
                     -> String -- ^ both strings concatenated, day zero padded
      prependYYYY_MM yyyy_mm line = yyyy_mm ++ "-" ++ ensure0 line
            where
              ensure0 (c:cs) | c == ' '  = '0':cs
                             | otherwise =  c :cs

-- chunks a list of lines into a list of tuples where in every tuple (a,b), a is the year and month line and `b` are the corresponding lines starting with day only
chunkIntoMonths :: [Line] -> [(Line,[Line])]
chunkIntoMonths [] = []
chunkIntoMonths (l:ls) = (l,month):chunkIntoMonths tail
  where
    (month,tail) = break isYYYY_MM ls

isYYYY_MM :: Line -> Bool
isYYYY_MM s = length s == 7
           && all isDigit (take 4 s)
           && (s!!4) == '-'
           && all isDigit ((take 2 . drop 5) s)
