#!/usr/bin/env stack
-- stack --resolver lts-12.5 script

{-
    removes everything after `#`
    lines starting with `#` are removed alltogether
    everything inside parentheses is interpreted as a comment and thus removed

    to compile: `stack ghc -- 0_remove_comments.hs`
                `./0_remove_comments example.shorthand`

-}

import Control.Applicative
import Data.List (isPrefixOf)
import System.Environment
import Text.Trifecta hiding (span)


main :: IO ()
main = do
  args <- getArgs
  file <- readFile (head args)
  let (coltypes:colnames:ls) = lines file
  let ls2 = dropTail ('#':tail colnames) ls
  let lp = coltypes:colnames:removeComments ls2
  mapM_ putStrLn lp

type Line = String

dropTail :: String -> [Line] -> [Line]
dropTail cue = takeWhile (not . isStop)
  where
    isStop line = startswith cue line || startswith "#EOF" line


removeComments :: [Line] -> [Line]
removeComments = map remCommentsPerLine . filter (not . isLineComment)
 where
   isLineComment = startswith "#" -- if we removed # regularly, we'd get empty line if # at start
                 
   remCommentsPerLine = removeHash . removeParens
    where
      removeHash   = takeWhile (/='#')
      removeParens :: String -> String
      removeParens line = s
         where
            Success s = p fc line
            p f = parseString f mempty
            fc :: Parser String
            fc = concat <$> sepBy1 (many (notChar '(')) (parens (many (notChar ')')))



startswith :: String -- ^ prefix
           -> String -- ^ word to match with prefix
           -> Bool
startswith = isPrefixOf

