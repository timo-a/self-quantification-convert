#!/usr/bin/env stack
-- stack --resolver lts-12.5 script

{-- to compile: `stack ghc -- 2_convert_to_csv.hs`
                `./2_convert_to_csv file.txt`
--}

{-# LANGUAGE LambdaCase #-}

import Control.Applicative

import Data.Function
import Data.List
import Data.List.Split (splitOn) -- hiding (sepBy)
import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.List.NonEmpty as NeList
import qualified Data.Map.Strict as Map
import Data.Maybe (mapMaybe)

import System.Environment
import Numeric (readHex)
import Text.Trifecta hiding (span)
import Text.Printf (printf)

{-
    The user defines which operations to apply on the columns by specifying a `[ColumnType]`
    in the first line of the table.
    The (if specified) last column contains several item that are parsed from a string.
    For this `ParseType` is specified.
-}

data ColumnType = Date String
                | TimeUp String -- ^ time but hous are hex to fit in one character
                | Time String
                | MultiFac [String] String --first list are the column names, second are list of possible abbrevs
                | BinaryFac String -- empty/blanks -> F. else T
                | HexMultiFac [String]
                | Parse [ParseType]
                deriving (Read, Show)

--to combat ambiguity we use PFac for ParseFactor if necessary
data ParseType = OneOfManyFac String    -- colname
                             [String]   -- factors
                            [[String]]  -- keys/abbreviations per factor
                             
               | BinaryPFac   String    -- colname
                             [String]   -- keys mean Yes
                             
               {- below not tested yet but compiles -}
               -- example for more complex parsertask e.g. sc1000 writes "1000" in column (sc is keyword)
               | DiscreteAmount String  -- colname
                               [String] -- keys that can come before the number

               {- one token encodes values for several columns
                  e.g. MultiPFac ["water_hugo", "water_anton"] "w" [["h","hugo"],["a","anton"]]
                  for watering two plants called anton and hugo, then "wha" or "whugo" or "wantonhugo",...     -}
               | MultiPFac [String]  -- column names
                            String   -- key
                          [[String]] -- keys per column 
               deriving (Read, Show)

-- get column name(s) for e.g. distinguishing items
getColName :: ParseType -> NonEmpty String
getColName (OneOfManyFac colname _ _) = colname :| []
getColName (BinaryPFac colname _)     = colname :| []
getColName (DiscreteAmount colname _) = colname :| []
getColName (MultiPFac colnames _ _)   = NeList.fromList colnames


type Cell = String
type Col  =  [Cell]
type Row  =  [Cell]
type Table= [[Cell]]


  

main :: IO ()
main = do
  args <- getArgs
  file <- readFile (head args)
  let (t:ls) = lines file
  let operations = read t :: [ColumnType]
  let table = map (splitOn ",") ls   -- ::[Row]
  let table2  = processTable operations (asCols table)
  mapM_ (putStrLn . intercalate ",") (asRows table2)

asRows :: [[String]] -> [[String]] --we only ever need it for Strings  
asRows = transpose
asCols :: [[String]] -> [[String]]
asCols = transpose

-- apply all operations on the respective columns
processTable :: [ColumnType] -> [Col] -> [Col]
processTable operations cols = concatMap NeList.toList (zipWith applyColumnOperation operations cols)

-- | apply an operation to a column
applyColumnOperation :: ColumnType    -- ^ operation to apply
                     -> Col           -- ^ column
                     -> NonEmpty Col  -- ^ at least one but maybe several columns

singleColumn :: Col -> NonEmpty Col
singleColumn c = c :| []
manyColumns :: [Col] -> NonEmpty Col
manyColumns = NeList.fromList

-- just replace the column name
applyColumnOperation (Date colname)   (_:col) = singleColumn (colname:col)
applyColumnOperation (Time colname)   (_:col) = singleColumn (colname:col)

-- time given as H:MM where `H` is a single digit hex number
-- turn into HH:MM
applyColumnOperation (TimeUp colname) (_:col) = singleColumn (colname:map unhexhour col)
  where
    unhexhour (h:hs) = h' ++ hs
       where
         h' = printf "%02d" (hexint::Int) :: String 
         [(hexint,"")] = readHex [h]   
    

-- | turn one column into several, e.g. rl into  read, speak, listen
--                                               O   ,      , O
applyColumnOperation (MultiFac columnNames keys) (_:col) = manyColumns
                                                         $ transpose
                                                         $ columnNames:map (map bool2str . checkAllFlags) col
  where
    checkAllFlags :: String -> [Bool]
    checkAllFlags cell = map (`elem` cell) keys
    

applyColumnOperation (BinaryFac colname) (_:col) = singleColumn (colname:map (bool2str . (/="")) col)

applyColumnOperation (HexMultiFac columnNames) (_:col) = manyColumns
                                                       $ transpose
                                                       $ columnNames : map split col
   where
     n = length columnNames
     template = concat ["%0", show n, "b"]

     split "" = replicate n ""
     split hex = map repl binary
      where
       repl '1' = "T"
       repl '0' = "F"
       binary = printf template (int :: Int) :: String 
       [(int,"")] = readHex hex   



applyColumnOperation (Parse parseOps) (_:col) = manyColumns
                                              $ transpose
                                              $ columnnames : map (tags2Row parsers columnnames) col
 where
   -- we don't want substrings to ruin the parse, always check for superstring first
   -- therefore sort descendingly
   parsersUnsorted :: [(String, Parser Parseresult)]
   parsersUnsorted = concatMap makeParserWithKey parseOps
   parsers :: [Parser Parseresult]
   parsers = map snd $ sortBy (flip compare `on` fst) parsersUnsorted
   columnnames :: [String]
   columnnames = concatMap (NeList.toList . getColName) parseOps
                 
applyColumnOperation _ _ = error "not implemented" --     

bool2str :: Bool -> String
bool2str = \case True -> "T"
                 False-> "F"



data Parseresult = ParseResult String -- column name
                               String -- value to save there
                 | Sep deriving (Show)

{- to avoid ambiguity we need to sort descending by keys, i.e. all sequences that  start a token,
   therefore we needd to explicitlly state key
-}
makeParserWithKey :: ParseType -> [(String,Parser Parseresult)]
makeParserWithKey (OneOfManyFac itemName levels abbrevss) = concat $ zipWith parseOneOfManyFac levels abbrevss
   where
     parseOneOfManyFac :: String -> [String] -> [(String, Parser Parseresult)]
     parseOneOfManyFac factorName abbrevs = [(key, const (ParseResult itemName factorName) <$> string key) | key <- abbrevs]
     
makeParserWithKey (BinaryPFac colname abbrevs) = map parseBinary abbrevs
   where
     parseBinary :: String -> (String, Parser Parseresult)
     parseBinary abb = (abb, const (ParseResult colname "T") <$> string abb)

makeParserWithKey (DiscreteAmount colname abbrevs) = map (\key -> (key, ParseResult colname <$> (string key *> many digit))) abbrevs

makeParserWithKey (MultiPFac colnames key colkeys) = concat $ zipWith perColumn colnames colkeys
   where
     perColumn :: String -> [String] -> [(String, Parser Parseresult)]
     perColumn colname keys = [(key, ParseResult colname <$> string (key++colkey)) | colkey <- keys]

-- | parse a string into an ordered seq of values
tags2Row :: [Parser Parseresult] -> [String] -> Cell -> Row
tags2Row parsers colnames = sortout . p (sepBy (choice parsers) parseSepTok)
 where
   sortout :: [Parseresult] -- ^ unordered parseresults
           -> [String]      -- values for the columns in the right order
   sortout rs = map (flip (Map.findWithDefault "") dict) colnames
    where
      dict = Map.fromList $ mapMaybe extractKV rs -- map from column name to value

   -- | extracts key and value from result of parser
   extractKV :: Parseresult -> Maybe (String, String)
   extractKV (ParseResult k v) = Just (k,v)
   extractKV  Sep              = Nothing -- shouldn't occur, parser should just swallow them
             
   p :: Parser [Parseresult] -> String -> [Parseresult]
   p f i = case parseString f mempty i of
        Success q -> q
        Failure q -> error $ concat ["input was: >", i, "<, errormsg: ", show q]


parseSepTok :: Parser [Parseresult]
parseSepTok = const [Sep] <$> choice [ char ' '
                                     , char ','
                                     , char ':'
                                     ]
